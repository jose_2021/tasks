import { Component } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Task, NewTask } from '../interfaces';
import { TasksService } from '../services/tasks.service';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../components/modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public tasks: Task[] = [];
  modalDataResponse: any;

  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    public toastController: ToastController, 
    public modalCtrl: ModalController,
    private tasksService: TasksService) {}

  ngOnInit(){
    const loading = this.presentLoading();
    this.tasksService.getTasks()
      .subscribe( (tasks) => {
        this.tasks.push( ...tasks.filter(task => task.status == true));
        this.loadingController.dismiss();
      });
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registrar nueva tarea',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Nombre de la tarea'
        },
        {
          name: 'description',
          type: 'textarea',
          placeholder: 'Descripción'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (alertData) => {    
            const loading = this.presentLoading();
            const message = "Tarea creada correctamente!!!";
            let data: NewTask;
            data = {
              name: alertData.name,
              description: alertData.description,
              status: true,
              _method: null
            }      
            if (alertData.name != "" && alertData.description != "") {
              this.tasksService.createTask(data)
                .subscribe((response) => {
                  this.tasksService.getTasks()
                    .subscribe((tasks) => {
                      this.tasks = [];
                      this.tasks.push( ...tasks.filter(task => task.status == true)); 
                      this.presentToast(message);          
                    },
                    (error) => {
                      loading.then(() => { this.loadingController.dismiss(); });
                    },
                    () => {
                      loading.then(() => { this.loadingController.dismiss(); });
                    });
                },
                (error) => {
                  loading.then(() => { this.loadingController.dismiss(); });
                });                
            } else {
              return false;
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async editPresentAlertPrompt(task:Task) {
    const alert = await this.alertController.create({      
      cssClass: 'my-custom-class',
      header: 'Editar tarea',
      inputs: [
        {
          name: 'name',
          type: 'text',
          value: task.name
        },
        {
          name: 'description',
          type: 'textarea',
          value: task.description
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (alertData) => {    
            const loading = this.presentLoading();
            const message = "Tarea editada correctamente!!!";
            let data: NewTask;
            data = {
              name: alertData.name,
              description: alertData.description,
              status: true,
              _method: null
            }      
            if (alertData.name != "" && alertData.description != "") {
              this.tasksService.updateTask(task.id, data)
                .subscribe((response) => {
                  this.tasksService.getTasks()
                    .subscribe((tasks) => {
                      this.tasks = [];
                      this.tasks.push( ...tasks.filter(task => task.status == true)); 
                      this.presentToast(message);          
                    },
                    (error) => {
                      loading.then(() => { this.loadingController.dismiss(); });
                    },
                    () => {
                      loading.then(() => { this.loadingController.dismiss(); });
                    });
                },
                (error) => {
                  loading.then(() => { this.loadingController.dismiss(); });
                });                
            } else {
              return false;
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    return await loading.present();
  }

  removeTask(taskId:Number) {
    const loading = this.presentLoading();
    const message = "Se ha eliminado la tarea!!!";
    this.tasksService.deleteTask(taskId)
      .subscribe(() => {
        this.tasksService.getTasks()
          .subscribe((tasks) => {
            this.tasks = [];
            this.tasks.push( ...tasks.filter(task => task.status == true));
            this.presentToast(message);             
          },
          (error) => {
            loading.then(() => { this.loadingController.dismiss();});
          },
          () => {
            loading.then(() => { this.loadingController.dismiss();});
          });
        }
      )
  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  async initModal(task:Task) {
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      componentProps: {
        'taskId': task.id
      }
    });

    modal.onDidDismiss().then((modalDataResponse) => {
      if (modalDataResponse !== null) {
        this.modalDataResponse = modalDataResponse.data;
      }
    });

    return await modal.present();
  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Task, NewTask } from '../interfaces';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) { }

  createTask(task: NewTask): Observable<any> {
    return this.httpClient.post<NewTask>(`${ apiUrl }/tasks`, task, this.httpOptions)
      .pipe(
        catchError(this.handleError<NewTask>('Error occured'))
      );
  }

  getTasks(): Observable<Task[]> {
    return this.httpClient.get<Task[]>(`${ apiUrl }/tasks`)
      .pipe(
        tap(tasks => console.log('Tasks retrieved')),
        catchError(this.handleError<Task[]>('Get user', []))
      );
  }

  getTask(id): Observable<Task> {
    return this.httpClient.get<Task>(`${ apiUrl }/tasks/${id}`)
      .pipe(
        tap(_ => console.log(`Task fetched: ${id}`)),
        catchError(this.handleError<Task>(`Get task id=${id}`))
      );
  }

  deleteTask(id): Observable<Task[]> {    
    return this.httpClient.delete<Task[]>(`${ apiUrl }/tasks/${id}`, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Task deleted: ${id}`)),
        catchError(this.handleError<Task[]>('Delete task'))
      );
  }

  updateTask(id, task: NewTask): Observable<any> {
    task._method = 'put';
    return this.httpClient.post(`${ apiUrl }/tasks/${id}`, task, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Task updated: ${id}`)),
        catchError(this.handleError<Task[]>('Update task'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  } 
}

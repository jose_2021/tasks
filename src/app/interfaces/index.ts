export interface Task {
    id: Number;
    name: string;
    description: string;
    status: Boolean;
    created_at: Date;
    update_at: Date;
}

export interface NewTask {
    name: string;
    description: string;
    status: Boolean;
    _method: string | null;
}
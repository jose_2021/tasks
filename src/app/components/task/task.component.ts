import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../interfaces/index';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {

  @Input() task: Task;
  @Input() index: number;
  @Output() deleteTask1 = new EventEmitter();
  @Output() editTask1 = new EventEmitter();
  @Output() showTask1 = new EventEmitter();

  constructor(public alertController: AlertController) { }

  ngOnInit() {}

  onDeleteTask(taskId) {
    this.deleteTask1.emit(taskId);
  }

  onEditTask(task:Task) {
    this.editTask1.emit(task);
  }

  onShowTask(task:Task) {
    this.showTask1.emit(task);
  }

  openSliding(slidingItem) {
    slidingItem.open();
  }

  async presentAlertConfirm(taskId) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar tarea',
      message: '¿Estás seguro de eliminar la tarea?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Si',
          id: 'confirm-button',
          handler: () => {
            this.onDeleteTask(taskId);
          }
        }
      ]
    });

    await alert.present();
  }

}

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from '../../interfaces/index';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit {

  @Input() tasks: Task[] = [];
  @Output() deleteTask = new EventEmitter();
  @Output() editTask = new EventEmitter();
  @Output() showTask = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  emitDeleteTask(taskId) {
    this.deleteTask.emit(taskId);
  }

  emitEditTask(task:Task) {
    this.editTask.emit(task);
  }

  emitShowTask(task:Task) {
    this.showTask.emit(task);
  }

}

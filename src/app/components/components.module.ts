import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TaskComponent } from './task/task.component';
import { TasksComponent } from './tasks/tasks.component';
import { ModalComponent } from './modal/modal.component';



@NgModule({
  declarations: [
    TaskComponent,
    TasksComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    TasksComponent,
    ModalComponent
  ]
})
export class ComponentsModule { }

import { Component, OnInit, Input } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { TasksService } from '../../services/tasks.service';
import { Task } from '../../interfaces/index';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  public showCard = false;
  public task: Task = {
    id: null,
    name: null,
    description: null,
    status: null,
    created_at: null,
    update_at: null
  };
  @Input() taskId: Number;
  @Input() name: string;

  constructor(
    public loadingController: LoadingController,
    private modalCtr: ModalController, 
    private tasksService: TasksService) { }

  ngOnInit() {
    const loading = this.presentLoading();
    this.tasksService.getTask(this.taskId)
      .subscribe((task) => {
        this.task = task;
        this.showCard = true;      
        this.loadingController.dismiss();
        }
      )
  }

  async close() {
    const closeModal: string = "Modal Closed";
    await this.modalCtr.dismiss(closeModal);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    return await loading.present();
  }

}
